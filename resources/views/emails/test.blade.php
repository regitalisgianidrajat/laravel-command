<h3>Hi {{$name}}!</h3>
<p> Here's the task from the section you requested </p>
<hr>
            <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Descriptions</th>
              </tr>
            </thead>
            <tbody>
            @php $no = 1; @endphp
            @foreach($data as $row)
            <tr>
              <td>{{$no++}}</td>
              <td>{{$row['task_name']}}</td>
              <td>{{$row['description']}}</td>
            </tr>
            @endforeach
            </tbody>
          </table>
<hr>

<p>Titik Pintar Test | Regita Lisgiani Drajat</p>
