<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//credential
Route::post('api/get-token','API\CredentialController@create');
// API
Route::group(['prefix' => 'api'], function () {
    //section
    Route::get('section/','API\SectionController@index');
    Route::get('section/{id}','API\SectionController@detail');
    Route::post('section/create','API\SectionController@store');
    Route::put('section/update','API\SectionController@update');
    Route::delete('section/{id}','API\SectionController@destroy');
    //task
    Route::get('task','API\TaskController@index');
    Route::get('task/{id}','API\TaskController@detail');
    Route::post('task/create','API\TaskController@store');
    Route::put('task/update','API\TaskController@update');
    Route::delete('task/{id}','API\TaskController@destroy');
    Route::post('task/search','API\TaskController@search');
    Route::post('task/status','API\TaskController@status');
});

