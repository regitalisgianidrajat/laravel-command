<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\SectionModels;
use App\Models\TaskModels;

class TaskTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testShouldCreateTask(){

        $data  = SectionModels::limit(1)->first();
        $parameters = [
            'section_id' => $data['section_id'],
            'task_name' => 'Infinix',
            'description' => 'NOTE 4 5.7-Inch IPS LCD (3GB, 32GB ROM) Android 7.0 ',
        ];

        $response = $this->post("api/task/create", $parameters, []);
        $response->assertStatus(200);
        
    }
}
