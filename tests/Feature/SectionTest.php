<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\SectionModels;

class SectionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetSection()
    {
        //multiple test case in one function
        $response = $this->get('/api/section');
        $response->assertStatus(200);
        $response
            ->assertStatus(200)
            ->assertJson([
                'STATUS' => true,
                'MESSAGE' => true,
                'DATA' => [
                    [
                        "section_id" => true,
                        "section_name" => true,
                        "description" => true,
                        "max_task" => true
                    ]
                ]
            ]);
        $data  = SectionModels::limit(1)->first();
        $response_detail = $this->get('/api/section/'.$data['section_id']);
        $response_detail->assertStatus(200);
        $response_detail
            ->assertStatus(200)
            ->assertJson([
                'STATUS' => true,
                'MESSAGE' => true,
                'DATA' => [
                        "section_id" => true,
                        "section_name" => true,
                        "description" => true,
                        "max_task" => true,
                        "task" => true
                    ]
            ]);
    }
    public function testShouldCreateSection(){

        $parameters = [
            'section_name' => 'Infinix',
            'description' => 'NOTE 4 5.7-Inch IPS LCD (3GB, 32GB ROM) Android 7.0 ',
            'max_task' => '10 ',
        ];

        $response = $this->post("api/section/create", $parameters, []);
        $response->assertStatus(200);
        
    }
}
