<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectionModels extends Model
{
    use SoftDeletes;

    protected $table   = 'section';
	public $primarykey = 'section_id';
    public $timestamps = true;
    protected $fillable = [
		'section_name',
		'description',
		'max_task',
	];
	protected $casts = [
		'section_name'  => 'string',
		'max_task' 	    => 'integer',
		'description' 	=> 'string'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at'
    ];
    public function task()
    {
        return $this->hasMany('App\Models\TaskModels','section_id', 'section_id');
    }
}
