<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskModels extends Model
{
    use SoftDeletes;

    protected $table   = 'task';
	public $primarykey = 'task_id';
    public $timestamps = true;
    protected $fillable = [
		'section_id',
        'task_name',
        'description',
        'status',
		'created_at'
	];
	protected $casts = [
		'section_id' 	=> 'integer',
		'task_name' 	=> 'string',
		'description' 	=> 'string',
		'status' 	=> 'integer',
    ];
		
	protected $hidden = [
		'updated_at',
		'deleted_at'
    ];

    public function section()
    {
        return $this->belongsTo('App\Models\SectionModels', 'task_id', 'section_id');
    }
}
