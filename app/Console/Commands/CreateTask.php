<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TaskModels;

class CreateTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $input['section_id']    = $this->ask('Please enter section id ');
        $input['task_name']     = $this->ask('Please enter task name ');
        $input['description']   = $this->ask('Please enter description for this task  ');

        $saved = TaskModels::create($input);

        if(!$saved){
            $this->info('Unable to create Task.');
        }
        $this->info('Task Create Successfully.');
    }
}
