<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\TaskModels;

class sendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $name = $this->ask('What is your name?');
        $section_id = $this->ask('Please enter section id ?');

        $data  = TaskModels::where('section_id',$section_id)->get();

        if ($data->isEmpty()) {
            $this->info('This section does not have any task!');
        }
        $data = array(
            'name' => $name,
            'data' => $data
        );

        Mail::send('emails.test', $data, function ($message) {

            $message->from(env('MAIL_FROM_ADDRESS'));
            $message->to($this->ask('What is your email?'))->subject('Task List');

        });
        $this->info('Hi '.$name.' the emails are send successfully! Please check your inbox/spam/promotion/social tab !');
    }
}
