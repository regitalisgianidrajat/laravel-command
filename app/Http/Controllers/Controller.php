<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function Validator(array $data, array $role, $first = true)
    {
        $validator = Validator::make($data, $role);
        if ($validator->fails()) {
            if (!$first) {
                $errors = $validator->getMessageBag()->first();

                foreach ($errors as $i=>$error) {
                    $errormsg[]['error'] = $error;
                }
                return $errormsg;
            }
            $errors = $validator->getMessageBag()->first();
            return $errors;
        }
    }
    
    protected function ResponseStatus($status_code, $message, $data)
    {
        return response()->json([
            'STATUS' => $status_code,
            'MESSAGE' => $message,
            'DATA' => $data
        ], $status_code);
    }
}
