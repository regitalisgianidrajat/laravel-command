<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\SectionModels;
use App\Models\TaskModels;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    private $section;
    private $task;

    public function __construct()
    {
        $this->section  = SectionModels::select('*');
        $this->task     = TaskModels::select('*')->orderBy('task_id', 'desc');
    }
    public function index()
    {
        $data  = $this->section->get();
        if (!$data->isEmpty()) {
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', array());
        }
        
    }
    public function detail($id)
    {
        $data  = $this->section->where('section_id',$id)->with('task')->first();

        if ($data) {
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', new \stdClass());
        }
        
    }
    public function store(Request $request)
    {
        $PostRequest = $request->only('section_name','description','max_task');
        $role = [
            'section_name' => 'Required',
            'max_task'      => 'Required',
            'description'  => 'Required'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('400', $ErrorMsg, new \stdClass());
        }
        
        $saved = SectionModels::create($PostRequest);

        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT SAVED', new \stdClass());
        }
        
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
    }
    public function update(Request $request)
    {
        $PostRequest = $request->only('section_name','description','max_task');
        $role = [
            'section_name'     => 'Required',
            'max_task'         => 'Required',
            'section_id'       => 'Required'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('500', 'FAILED '.$ErrorMsg, new \stdClass());
        }
        $PostRequest['description'] = $request['description'];
        $saved = SectionModels::where('section_id',$request['section_id'])->update($PostRequest);

        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT SAVED', new \stdClass());
        }
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
        
    }
    public function destroy($id)
    {
        $saved = $this->section->where('section_id',$id)->delete();
        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT DELETED', new \stdClass());
        }
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
        
    }

}
