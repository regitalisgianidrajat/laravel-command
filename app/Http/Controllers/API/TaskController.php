<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\TaskModels;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TaskController extends Controller
{
    private $task;

    public function __construct()
    {
        $this->task     = TaskModels::select('*')->orderBy('task_id', 'desc');
    }
    public function index(Request $request)
    {
        $data  = $this->task->where('section_id',$request['section_id'])->get();
        if (!$data->isEmpty()) {
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', array());
        }
        
    }
    public function detail($id)
    {
        $data  = $this->task->where('task_id',$id)->with('section')->first();
        if ($data) {
            // $data->created_at = $data->created_at->diffForHumans();
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', new \stdClass());
        }
        
    }
    public function store(Request $request)
    {
        $PostRequest = $request->only('section_id','task_name','description');
        $role = [
            'section_id'    => 'Required',
            'task_name'     => 'Required',
            'description'   => 'Required'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('400', $ErrorMsg, new \stdClass());
        }
        
        $saved = TaskModels::create($PostRequest);

        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT SAVED', new \stdClass());
        }
        
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
    }
    public function update(Request $request)
    {
        $PostRequest = $request->only('section_id','task_name','description');
        $role = [
            'task_id'       => 'Required',
            'section_id'    => 'Required',
            'task_name'     => 'Required'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('400', $ErrorMsg, new \stdClass());
        }
        if (!empty($ErrorMsg)) {
            return Redirect::back()->withErrors([$ErrorMsg]);
        }
        $PostRequest['description'] = $request['description'];
        $saved = TaskModels::where('task_id',$request['task_id'])->update($PostRequest);

        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT SAVED', new \stdClass());
        }
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
        
    }
    public function destroy($id)
    {
        $saved = $this->task->where('task_id',$id)->delete();
        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT DELETED', new \stdClass());
        }
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
        
    }
    public function status(Request $request)
    {
        $PostRequest = $request->only('status');
        $role = [
            'task_id'       => 'Required',
            'status'        => 'Required'
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('400', $ErrorMsg, new \stdClass());
        }
        if (!empty($ErrorMsg)) {
            return Redirect::back()->withErrors([$ErrorMsg]);
        }
        $saved = TaskModels::where('task_id',$request['task_id'])->update($PostRequest);

        if(!$saved){
            return $this->ResponseStatus('500', 'FAILED DATA NOT SAVED', new \stdClass());
        }
        return $this->ResponseStatus('200', 'SUCCESS', new \stdClass());
        
    }
    public function search(Request $request)
    {
        
        $data  = $this->task->where('task_name', 'like', '%' . $request['task_name'] . '%')->with('section')->get();
        if(!empty($request['status'])){
            $data  = $this->task->where('task_name', 'like', '%' . $request['task_name'] . '%')->where('status',$request['status'])->with('section')->get();
        }

        if (!$data->isEmpty()) {
            return $this->ResponseStatus('200', 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', new \stdClass());
        }
        
    }
    

}
