<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\SectionModels;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(0,10) as $i){
            SectionModels::create([
                'section_name' => $faker->name,
                'description' => $faker->sentence(1),
                'max_task' => '10'
            ]);
        }
    }
}
