# Titik Pintar Backend Developer Test

### Developer
* Regita Lisgiani

### Built With
* [Laravel](https://laravel.com/)

## Getting Started

First clone the project into your local machine

### Installation

1. Clone the project 
```sh
git clone https://gitlab.com/regitalisgianidrajat/laravel-command.git
```
2. Whenever you clone a new Laravel project you must now install all of the project dependencies. This is what actually installs Laravel itself, among other necessary packages to get started.When we run composer, it checks the composer.json file which is submitted to the github repo and lists all of the composer (PHP) packages that your repo requires. Because these packages are constantly changing, the source code is generally not submitted to github, but instead we let composer handle these updates. So to install all this source code we run composer with the following command.
```sh
composer install
```
3. Create env (env that i use is already copied at .env.example file)  
4. create database and start run the terminal then type
 ```sh
php artisan migrate
php artisan db:seed
```
5. Start to view, create, update, or delete by hit the public url (/public/routes_name)

### API

1. Import the collection into your postman [Titik Pintar](https://www.getpostman.com/collections/f27e65311e989afaad89)
2. Make env for postman collection - endpoint url for this api example [URL](localhost/courses/TitikPintar/public/api/)
```sh
//this is example env for the postman
{
	"id": "#",
	"name": "TitikPintar",
	"values": [
		{
			"key": "URL",
			"value": "localhost/courses/TitikPintar/public/api/",
			"enabled": true
		}
	]
}


Note 
Im not finished all the task but heres what ive finished
1. The user can show, create, update, or delete a section.
2. The user can create, update, or delete a task.
3. The user can show tasks of a section.
4. The user can change the state of the task from todo to done or from done to todo.
5. The user can filter the tasks by the state.
6. The user can search a task
8. The tasks must be chronologically displayed within a section. The newest created task
comes first.
9. Each section or task must be accessible via an URL. (ie: /sections/34,
/sections/34/tasks/24, /tasks/24)
10. Create fixtures and/or seeders with some randomness. (is it mean using seeder
12. The user can also create a task from a command in the command line.
13. The user can also change the status of a task from the command line.
14. Create a command-line that will send by email the integrality of the task list to
nsa@example.net

-i also using DDD concept and some of tdd using laravel unit testing for test some of API
-the task that i couldnt complete is
7. Show timestamp of created task in human format readable time (ie: 1 hour ago)
11. Add the caching mechanism of your choice.
15. The user can undo an action he did on a task or a section.

the reason is the time and also some errors

